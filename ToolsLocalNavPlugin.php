<?php

/**
 * 
  *  Bolotweet-Tools
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Jorge J. Gomez Sanz <jjgomez@ucm.es>
 *
 */

require_once INSTALLDIR . '/lib/util.php';

if (!defined('STATUSNET')) {
    exit(1);
}



class ToolsLocalNavPlugin extends Plugin {

    function onInitializePlugin() {
        // A chance to initialize a plugin in a complete environment
    }

    function onCleanupPlugin() {
        // A chance to cleanup a plugin at the end of a program
    }


    function onStartDefaultLocalNav($action){ 
    
    $user = common_current_user();
     if (!empty($user)) {
    
                $action->elementStart('li');
                $action->element('h3', null, _m('Tools'));
                $action->elementStart('ul', array('class' => 'nav'));
                Event::handle('StartToolsLocalNav', array($action));
                Event::handle('EndToolsLocalNav', array($action));
                $action->elementEnd('ul');
                $action->elementEnd('li');
               
            }

        return true;
    }




    function onPluginVersion(array &$versions) {
        $versions[] = array('name' => 'ToolsLocalNav',
            'version' => 1.0,
            'author' => 'Jorge Gomez',
            'rawdescription' =>
            _m('A plugin to create a section for teaching tools'));
        return true;
    }

}
